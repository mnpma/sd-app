package com.hm.services;

import com.hm.entities.Contact;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author mpimenova
 */
public class ContactControllerTest {
    private static final String DATE_TIME_STRING = "1980-09-16";
    private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd";

    @Mock private ContactService contactService;
    @Mock private MessageSource messageSource;

    @InjectMocks
    private ContactController contactController = new ContactController();
    private Contact contact;

    @Before
    public void setUp() {
        initMocks(this);
        DateTime dateTime = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT_PATTERN);
        dateTime = formatter.parseDateTime(DATE_TIME_STRING);

        contact = new Contact();
        contact.setId(11L);
        contact.setFirstName("fn");
        contact.setLastName("ln");
        contact.setBirthDate(dateTime);
    }

    @Test
    public void testShow() {
        when(contactService.findById(anyLong())).thenReturn(contact);
        ExtendedModelMap uiModel = new ExtendedModelMap();
        contactController.show(11l, uiModel);

        assertEquals(1, uiModel.size());
        Contact modelContact = (Contact)uiModel.get("contact");
        assertNotNull(modelContact);
    }
}
