package com.hm.rest.utils;

import org.exolab.castor.mapping.GeneralizedFieldHandler;
import org.exolab.castor.mapping.ValidityException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;
import java.util.Properties;

/**
 * @author mpimenova
 */
public class DateTimeFieldHandler extends GeneralizedFieldHandler {
    private static String dateFormatPattern;

    @Override
    public void setConfiguration(Properties config) throws ValidityException {
        dateFormatPattern = config.getProperty("date-format");
    }

    @Override
    public Object convertUponGet(Object o) {
        DateTime dateTime = (DateTime) o;
        return format(dateTime);
    }

    @Override
    public Object convertUponSet(Object o) {
        String dateTimeString = (String)o;
        return parse(dateTimeString);
    }

    @Override
    public Class getFieldType() {
        return DateTime.class;
    }

    protected static String format(final DateTime dateTime) {
        String dateTimeString = "";
        if (dateTime != null) {
            DateTimeFormatter formatter = DateTimeFormat.forPattern(dateFormatPattern);
            dateTimeString = formatter.print(dateTime);
        }
        return dateTimeString;
    }

    protected  static DateTime parse(final String dateTimeString) {
        DateTime dateTime = new DateTime();
        if(dateTimeString != null) {
            DateTimeFormatter formatter = DateTimeFormat.forPattern(dateFormatPattern);
            dateTime = formatter.parseDateTime(dateTimeString);
        }
        return  dateTime;
    }
}
