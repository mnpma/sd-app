package com.hm.rest;

import com.hm.entities.Contact;
import com.hm.services.ContactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author mpimenova
 */
@Controller
@RequestMapping(value="/services/contact")
public class ContactRestController {
    private final Logger logger = LoggerFactory.getLogger(ContactRestController.class);
    @Autowired
    private ContactService contactService;

    @RequestMapping(value = "/listdata", method = RequestMethod.GET)
    @ResponseBody
    public Contacts listData() {
        return new Contacts(contactService.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Contact findContactById(@PathVariable Long id) {
        return contactService.findById(id);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public Contact create(@RequestBody Contact contact) {
        logger.info("creating contact.");
        contactService.save(contact);
        return  contact;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public void update(@RequestBody Contact contact, @PathVariable Long id) {
        logger.info("updating contact");
        contactService.save(contact);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable Long id) {
        logger.info("delete contact");
        Contact contact = contactService.findById(id);
        contactService.delete(contact);
    }


    public void setContactService(ContactService contactService) {
        this.contactService = contactService;
    }
}
