package com.hm.rest;

import com.hm.entities.Contact;

import java.io.Serializable;
import java.util.List;

/**
 * @author mpimenova
 */
public class Contacts implements Serializable{
    private List<Contact> contacts;
    public Contacts(){}

    public Contacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }
}
