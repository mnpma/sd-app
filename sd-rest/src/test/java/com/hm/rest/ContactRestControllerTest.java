package com.hm.rest;

import com.hm.entities.Contact;
import com.hm.services.ContactService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author mpimenova
 */
public class ContactRestControllerTest {
    private static final String DATE_TIME_STRING = "1980-09-16";
    private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd";
    private final List<Contact> contacts = new ArrayList<Contact>();

    private ContactRestController contactRestController;
    @Mock
    private ContactService contactService;

/*
    @BeforeClass
    public void initClass(){
        int y = 0;
    }
*/

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        contactRestController = new ContactRestController();
        contactRestController.setContactService(contactService);
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT_PATTERN);
        DateTime dateTime = formatter.parseDateTime(DATE_TIME_STRING);

        Contact contact = new Contact();
        contact.setId(11L);
        contact.setFirstName("fn");
        contact.setLastName("ln");
        contact.setBirthDate(dateTime);
        contacts.add(contact);
    }

    /*expected = Throwable.class*/
    @Test()
    public void testList() {
        when(contactService.findAll()).thenReturn(contacts);
        ExtendedModelMap uiModel = new ExtendedModelMap();
        uiModel.addAttribute("contacts", contactRestController.listData());

        Contacts modelContacts = (Contacts)uiModel.get("contacts");
        assertEquals(1, modelContacts.getContacts().size());

    }
}
