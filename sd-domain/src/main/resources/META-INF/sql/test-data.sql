insert into contact(firstName, lastName, birthDate)
 values('Chris', 'Last1', '1980-09-09');
insert into contact(firstName, lastName, birthDate)
 values('Scott', 'Last2', '1980-09-10');
insert into contact(firstName, lastName, birthDate)
 values('John', 'Last3', '1980-09-11');
insert into contact(firstName, lastName, birthDate)
 values('Mary', 'Last4', '1980-09-08');

insert into contact_tel_detail(contactId, telType, telNumber)
 values(1, 'mobile', '1234567890');
insert into contact_tel_detail(contactId, telType, telNumber)
 values(1, 'home', '1234567890');
insert into contact_tel_detail(contactId, telType, telNumber)
 values(2, 'home', '1234567890');
insert into contact_tel_detail(contactId, telType, telNumber)
 values(3, 'home', '1234567890');
insert into contact_tel_detail(contactId, telType, telNumber)
 values(4, 'home', '1234567890');

insert into hobby(hobbyId) values('jogging');
insert into hobby(hobbyId) values('programming');
insert into hobby(hobbyId) values('reading');
insert into hobby(hobbyId) values('swimming');

insert into contact_hobby_detail(contactId, hobbyId) values(1, 'jogging');
insert into contact_hobby_detail(contactId, hobbyId) values(1, 'reading');
insert into contact_hobby_detail(contactId, hobbyId) values(2, 'swimming');
