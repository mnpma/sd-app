create table contact (
 id int not null auto_increment
 , firstName varchar(60) not null
 , lastName varchar(40) not null
 , birthDate date not null
 , description varchar(2000)
 , photo blob
 , version int not null default 0
 , unique uq_contact_1 (firstName, lastName, birthDate)
 , primary key (id)
);
create table contact_tel_detail (
 id int not null auto_increment
 , contactId int not null
 , telType varchar(20) not null
 , telNumber varchar(20) not null
 , version int not null default 0
 , unique uq_contact_tel_detail_1 (contactId, telType)
 , primary key (id)
 , constraint fk_contact_tel_detail_1 foreign key (contactId) references contact(id)
);
create table hobby (
 hobbyId varchar(20) not null
 , primary key (hobbyId)
);
create table contact_hobby_detail (
 contactId int not null
 , hobbyId varchar(20) not null
 , primary key (contactId, hobbyId)
 , constraint fk_contact_hobby_detail_1 foreign key (contactId) references contact(id)
 , constraint fk_contact_hobby_detail_2 foreign key (hobbyId) references hobby(hobbyId)
);