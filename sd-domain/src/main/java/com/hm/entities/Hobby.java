package com.hm.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author mpimenova
 */
@Entity
@Table(name="hobby")
public class Hobby implements Serializable {
    private String hobbyId;
    private Set<Contact> contacts = new HashSet<Contact>();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public String getHobbyId() {
        return hobbyId;
    }

    public void setHobbyId(String hobbyId) {
        this.hobbyId = hobbyId;
    }
    @ManyToMany
    @JoinTable(name="contact_hobby_detail",
    joinColumns = @JoinColumn(name="hobbyId"),
    inverseJoinColumns = @JoinColumn(name="contactId"))
    public Set<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }
}
