package com.hm.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author mpimenova
 */
@Entity
@Table(name="contact_tel_detail")
public class ContactTelDetail implements Serializable {
    private Long id;
    private String telType;
    private String telNumber;
    private int version;
    private Contact contact;

    public ContactTelDetail() {}

    public ContactTelDetail(String telType, String telNumber) {
        this.telType = telType;
        this.telNumber = telNumber;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTelType() {
        return telType;
    }

    public void setTelType(String telType) {
        this.telType = telType;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }
    @Version
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    @ManyToOne
    @JoinColumn(name="contactId")
    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
