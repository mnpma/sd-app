package com.hm.dao.impl;

import com.hm.dao.ContactDao;
import com.hm.entities.Contact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author mpimenova
 */
@Repository
@Transactional
public class ContactDaoImpl implements ContactDao {
    private final static String FIND_ALL_QUERY = "select c from Contact c";
    private final static String FIND_ALL_WITH_DETAILS_QUERY = "select distinct c from Contact c " +
            " left join fetch c.contactTelDetails t " +
            " left join fetch c.hobbies h " ;
    private final static String FIND_BY_ID_QUERY = "select c from Contact c " +
            " where c.id = :id " ;
    private Logger logger = LoggerFactory.getLogger(ContactDaoImpl.class);
    @PersistenceContext
    private EntityManager em;
    @Transactional(readOnly = true)
    @Override
    public List<Contact> findAll() {
        Query query = em.createQuery(FIND_ALL_QUERY);
        return query.getResultList();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Contact> findAllWithDetails() {
        Query query = em.createQuery(FIND_ALL_WITH_DETAILS_QUERY);
        return query.getResultList();
    }

    @Transactional(readOnly = true)
    @Override
    public Contact findById(Long id) {
        TypedQuery<Contact> query = em.createQuery(FIND_BY_ID_QUERY, Contact.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    @Override
    public Contact save(Contact contact) {
        if(contact.getId() == null) {
            logger.info("inserting new contact");
            em.persist(contact);
        } else {
            em.merge(contact);
            logger.info("updating existing contact");
        }
        return contact;
    }

    @Override
    public void delete(Contact contact) {
        Contact mergedContact = em.merge(contact);
        em.remove(mergedContact);
        logger.info("contact with id: " + contact.getId() + " deleted successfully");
    }
}
