### Contacts List

#### Build and Run tests:
```
cd sd-app
mvn clean install
```

#### Run

##### Pre-condition

*Tomcat should be installed on computer

*SET CATALINA_HOME

```
execute sd-root.bat
```

#### URL
```
    http://localhost:8080/sd-root-1.0/contacts/
```

