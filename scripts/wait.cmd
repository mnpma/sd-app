rem wait.cmd
@echo off
set wait=%1
echo waiting %wait% s
echo wscript.sleep %wait%000 > wait.vbs
wscript.exe wait.vbs
del wait.vbs
rem timeout command isn't available on Windows XP. It works on Windows 2003 and Windows 7 though. 