set APP_DIR=sd-app
set APP_NAME=sd-root
set APP_FILE_NAME=sd-root-1.0

set APP_HOME=%APP_DIR%\%APP_NAME%
echo %APP_HOME%

set CATALINA_HOME=C:\apache-tomcat-7.0.39
set CATALINA_HOME_APP=%CATALINA_HOME%

set SCRIPTS_EXTENDED=scripts

call mvn  clean install -Dmaven.test.skip=true
if errorlevel 1 pause

rem mvn org.apache.maven.plugins:maven-dependency -plugin:2.8:get -Dartifact=com.exigen.ipb.billing:ipb-billing-ui:5.2.0:jar:sources

echo OFF
echo ---Delete logs---
set APP_LOG=%CATALINA_HOME%\logs\*.*
echo %APP_LOG% 
del /Q %APP_LOG%
if exist %CATALINA_HOME_APP%\logs rd /S /Q %CATALINA_HOME_APP%\logs
echo recreate DIR %CATALINA_HOME_APP%\logs
md %CATALINA_HOME_APP%\logs

rem remove localhost DIRECTORY
rd /S /Q %CATALINA_HOME_APP%\work\Catalina\localhost

del /Q %CATALINA_HOME_APP%\temp\*.*
if exist %CATALINA_HOME_APP%\temp rd /q /s %CATALINA_HOME_APP%\temp
mkdir %CATALINA_HOME_APP%\temp

if exist %CATALINA_HOME_APP%\work rd /q /s %CATALINA_HOME_APP%\work
mkdir %CATALINA_HOME_APP%\work


if exist %CATALINA_HOME_APP%\webapps\%APP_FILE_NAME% rd /s /q %CATALINA_HOME_APP%\webapps\%APP_FILE_NAME%

echo --- Delete wars---
set CATALINA_WORK_FILE=%CATALINA_HOME_APP%\webapps\%APP_FILE_NAME%.war
echo %CATALINA_WORK_FILE%

if exist %CATALINA_WORK_FILE% del /Q %CATALINA_WORK_FILE%
if errorlevel 1 pause

rem deploy
echo %APP_HOME%\target\%APP_FILE_NAME%.war

cd %APP_HOME%\target
copy /Y %APP_FILE_NAME%.war /B %CATALINA_WORK_FILE%
if errorlevel 1 pause

cd ..\..

rem run TOMCAT
echo ---Tomcat will be started---
call %CATALINA_HOME_APP%\bin\catalina.bat jpda start
call %SCRIPTS_EXTENDED%\wait.cmd 22
rem call wscript.exe %SCRIPTS_EXTENDED%\playSound.vbs
call wscript.exe %SCRIPTS_EXTENDED%\outputInfo.vbs
