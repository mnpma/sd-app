package com.hm.services;

import com.hm.entities.Contact;

import java.util.List;

/**
 * @author mpimenova
 */
public interface ContactService {
    /**
     * Find all contacts
     * @return list of contacts
     */
    List<Contact> findAll();

    /**
     * Find all contacts with detail(telephone number etc)
     * @return list of contacts
     */
    List<Contact> findAllWithDetails();

    /**
     * Find contact by id
     * @param id
     * @return found contact
     */
    Contact findById(Long id);

    /**
     * Save or Update contact
     * @param contact
     * @return processed contact
     */
    Contact save(Contact contact);

    /**
     * Delete contact and all details
     * @param contact
     */
    void delete(Contact contact);

}
