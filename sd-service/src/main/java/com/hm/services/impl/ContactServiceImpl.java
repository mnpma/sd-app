package com.hm.services.impl;

import com.hm.dao.ContactDao;
import com.hm.entities.Contact;
import com.hm.services.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author mpimenova
 */
@Component("contactService")
public class ContactServiceImpl implements ContactService {
    @Autowired
    @Qualifier("contactDao")
    ContactDao contactDao;
    @Override
    public List<Contact> findAll() {
        return contactDao.findAll();
    }

    @Override
    public List<Contact> findAllWithDetails() {
        return contactDao.findAllWithDetails();
    }

    @Override
    public Contact findById(Long id) {
        return contactDao.findById(id);
    }

    @Override
    public Contact save(Contact contact) {
        return contactDao.save(contact);
    }

    @Override
    public void delete(Contact contact) {
        contactDao.delete(contact);
    }

    public void setContactDao(ContactDao contactDao) {
        this.contactDao = contactDao;
    }
}
